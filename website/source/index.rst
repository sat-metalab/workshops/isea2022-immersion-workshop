.. container:: index-title

   Creating Spontaneous and Mobile Immersive spaces using FLOSS

.. container:: index-intro

   a workshop by [SAT] Metalab

.. container:: index-menu

   `-` `about us <https://sat.qc.ca/fr/recherche/metalab>`__ - `our tools <https://sat-metalab.gitlab.io>`__ -

.. container:: index-section

    what is this workshop about

This workshop is aimed at artists, designers, content creators and other creatives who are interested in creating immersive spaces using low cost and light equipment. We will be using off-the-shelf hardware and open-source software to create an interactive and immersive experience, from scratch, in an impromptu space. While doing so, we will be learning about the tools and the process of getting the basic elements of such undertaking, such as evaluating the available space, setting up projection mapping, calibrating video projectors and sound equipment, setting up cameras for live pose tracking and estimation and employing a software pipeline to glue it all together.

The workshop is based on using exclusively free/libre open source software democratizing what's possible to do with computer vision, computer graphics and spatialized audio technologies that are still marginalized and on the fringes of creative activities.

We will be exploring production pipelines that illustrate the use of software developed by the Metalab in tandem with existing tools that have been seeing wider adoption in recent years. For clarity, we present the proposed toolset in two groups: software developed by the Metalab and other software.

-   Metalab software:
  -   `LivePose <https://sat-metalab.gitlab.io/livepose/>`__ - for interpreting live pose estimation using machine learning
  -   `Splash <https://sat-metalab.gitlab.io/splash/>`__ - for video mapping
  -   `SATIE <https://sat-metalab.gitlab.io/satie>`__ - for sound rendering and spatialization
-   Other software:
  -   `Godot Game Engine <https://godotengine.org/>`__ - live 3D renderer for interactive content
  -   `Chataigne <https://benjamin.kuperberg.fr/chataigne/en>`__ - show control and "glue" for data mapping
-   Hardware:
  -   `NVIDIA Jetson Xavier NX <https://www.nvidia.com/en-us/autonomous-machines/embedded-systems/jetson-xavier-nx/>`__
  -   `Raspberry Pi <https://www.raspberrypi.org/>`__

The above software can run on low-cost, low-energy hardware such as Raspberry Pi and NVIDIA Jetson single board computers. We believe that putting such hardware and software into creative hands can bring awe an wonder as much, if not more, than the technological bloat that we experience today.

.. container:: index-section

    workshop plan

This workshop is intended to be collaborative, almost a hackathon! With the help of the workshop instructors, the attendees will create a collaborative installation that features video projection mapping, sound spatialization and interaction, while learning about the techniques involved in achieving different steps.
There is no minimum skill requirement to attend, just curiosity and desire to learn one possible production pipeline that can be reproduced without buying expensive software. We welcome participation with creating artworks, assets and planning, so that everyone can get their feet wet and hands dirty. Figuratively, at least.

Here's the break-out of the workshop

-  Day One
  -   Getting to know each other's skills
  -   Brainstorming the creative immersive experience
  -   Learning about the steps involved in the creation of an immersive and interactive work
  -   Discovering the tools
  -   Assets creation and story boarding
  -   Rapid prototyping when possible
-   Day Two:
  -   Deep dive into the tools
  -   Refining the goals
  -   Integration
  -   Tweaking, detailing
  -   Experiencing the work
  -   Public presentation (if possible)


.. container:: index-section

   previous collaborative workshops

-   Edition in situ:

.. raw:: html

  <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/345694399?h=2d9c2bcfab&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="iX 2019 - EiS workshop"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

-   Rapid Prototyping of Immersive Audiovisual Installation

.. raw:: html

  <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/375956319?h=822a6fba77&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="Rapid Prototyping of Immersive Audiovisual Installation @Centro de Cultura Digital"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

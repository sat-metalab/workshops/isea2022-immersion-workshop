# ISEA 2022 - Creating Spontaneous and Mobile Immersive spaces using FLOSS

## Notes from the workhop

### Organization
* one morning of presentation (see the slides)
* one afternoon of devising what to do
    * the definition of the final experience is nothing too precise
    * left a lot of room for each participant to bring what they are able to bring
* one morning to setup the tools
* the rest for plumbing and sharing experience with participants and visitors

### Participation
* needed some activities to "jump-start" the participants
* Jenn took care of that by using some mindfulness (?) exercices
    * this allowed for participants to talk about the interactivity
    * which in turn made them more confortable with what they could deal with

### Technicalities
* coming with software-bundled-in-hardware works very well
* the Optoma LED projectors need darkness, unless very close to the surface
* a lot was made possible by the use of the network
    * thumbs up for bringing a network switch
    * long cable are very useful, could have checked with org before
* sharing feeds allows for new possibilities regarding collaboration

### Comments from the participants (and others)
* very interesting to have access to such tools as FLOSS
* the ability to use their own tool and just send/receive feeds really talk to participants
* the ability to send feeds between participants was new for participants
* importance of tools to share/map feeds of all sorts

### Remarks
we started off with 11 people which were sort of organically divided in 4 groups (3+3+3+2). Another person arrived in the afternoon. Each group brainstormed a project idea. They all came up with similar concept: live tracking presence and proximity of people and doing something visually based on this information. We narrowed down to one collective project while keeping some of the individual ideas.

### Final installation
Hash → John
Bruna → John
Laura → John
John → Splash
Bruna → Splash
LivePose → Bruna
LivePose → John
LivePose → audio people
audio people → SATIE

### Tools used:
- Participants:
    - Ableton
    - Max
    - Unity
    - TouchDesigner
    - Blender
- Metalab:
    - Splash
    - LivePose
    - SATIE
    - Score

![workshop network](./img/ISEA2022_workshop_network.png)

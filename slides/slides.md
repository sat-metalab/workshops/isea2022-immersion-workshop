---
title: "Creating Spontaneous and Mobile Immersive Spaces using FLOSS | ISEA 2022 Workshop"
theme: sky
separator: <!--section-->
verticalSeparator: <!--slide-->

revealOptions:
  transition: "none"
  slideNumber: "c/t"
  hashOneBasedIndex: true
  history: true
  margin: 0.01
  controls: true
  progress: true
  dependencies:
    - src: "module/reveal.js-plugins/chart/Chart.min.js"
    - src: "module/reveal.js-plugins/chart/csv2chart.js"
    - src: "module/reveald3/reveald3.js"
    - src: "module/reveal.js-plugins/spreadsheet/ruleJS.all.full.min.js"
    - src: "module/reveal.js-plugins/spreadsheet/spreadsheet.js"
    - src: "module/reveal-a11y/accessibility/helper.js"
---

<!-- Use this to add a background image: -->

<!-- Use this transparent styling in case of a background image: -->
<div class="title">

<h2>Creating Spontaneous and Mobile <br/>Immersive Spaces using FLOSS</h2>

### ISEA 2022 Workshop

<!-- <div class="affiliations"> -->
<!-- <div class="affiliation"> -->
<!-- </div> -->

<br/>

<!-- </div>  -->

<div class="authors">

<div class="author">
<img src="images/portraits/MichalSeta.jpg" alt="Michał Seta"/>
<div class="name">
<a href="mailto:mseta@sat.qc.ca" title="Michał Seta's Email">Michał</a></span> <a href="http://djiamnot.xyz" title="Michał Seta's Website">Seta</a></span>
</div>
</div>

<div class="author">
<img src="images/portraits/EmmanuelDurand.png" alt="Emmanuel Durand"/>
<div class="name">
<a href="mailto:edurand@sat.qc.ca" title="Emmanuel Durand's Email">Emmanuel</a></span> <a href="https://emmanueldurand.net" title="Emmanuel Durand's Website">Durand</a></span>
</div>
</div>

</div>

<br/>

<div class="affiliations">
<div class="affiliation">
<a class="logo" href="https://sat.qc.ca" title="Society for Arts and Technology"><img src="images/logos/SAT.png" alt="Société des Arts Technologiques"/></a>
</div>
</div>

#### 2022-06-11

</div class="title">


<!--section-->

# Welcome

<!--slide-->

## Workshop presentation

* **Goal**: make use of FOSS to create an immersive piece
* **Note**: participants will be split into groups

<!--slide-->

## Presenters and the SAT

![](images/sat_urbania_2.png)

<!--slide-->

### Immersion in fulldomes

![](images/recherche-artistique3.jpg)

<!--slide-->

### Immersion outside of fulldomes

![](images/authoring-menu-2017.jpg)

<!--slide-->

## Round table

* Biography in three sentences.
* Why do you bring to this workshop?

<!--section-->

# Workshop summary

<!--slide-->

## Day 1

* Getting to know each other
* Brainstorming what your creation will be
* Setting up the technicalities
* Experimenting possibilities

<!--slide-->

## Day 2

* Continuing with experimentation
* Creating and assembling content
* Discuss, improve, imagine

<!--section-->

# About immersion

<!--slide-->

## Audio-visual immersion

### What is audio-visual immersion?

<!--slide-->

## Immersive interaction

### What are some challenges of immersive interaction?

<!--slide-->

## Collective immersion

### What makes collective immersion different from individual immersion?

<!--section-->

# Available tools

<!--section-->

## Hardware tools

* LED projectors
* Depth cameras
* Lightweight/embedded computers
* Sound card and associated audio speakers
* ... whichever tool you brought!

<!--section-->

## Software tools

Metalab tools cover:

* Audio spatialization
* Projection mapping
* Real time pose detection

<!--section-->

### Spatial audio with SATIE

![](images/logos/satie.png)

<!--slide-->

#### SATIE

* synchronised with 3D engines
* interfaces with many speakers
* simultaneous rendering
* multitude of sound sources

<!--slide-->

#### SATIE - why?

sound object

<iframe src="https://player.vimeo.com/video/398346954?h=d4ab3f4b2a#t=0m37s&autoplay=1" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

<!--slide-->

#### SATIE is for:

* possible sound sources:
  * live input
  * synthesis
* effets

<!--slide-->

#### SATIE - how?

* OSC (Open Sound Control)
* Applications:
  * Godot Game Engine
  * Unity3D
  * Blender
  * Max(4Live), Pure Data, etc...

<!--slide-->

#### Demo

Here are some examples of SATIE in action:

<iframe src="https://player.vimeo.com/video/616508216?autoplay=1" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

<!--section-->

### Projection mapping with Splash

![](images/logos/splash.png)

<!--slide-->

#### About projection mapping

"Projection Mapping uses everyday video projectors, but instead of projecting on a flat screen (...), light is mapped onto any surface (...)"

-- <cite>[http://projection-mapping.org/what-is-projection-mapping/](http://projection-mapping.org/what-is-projection-mapping/)</cite>

<!--slide-->

#### Projecting onto arbitrary surfaces

![](images/realObjectWithVideo.jpg)

<!--slide-->

#### Projecting onto arbitrary surfaces

![](images/realObjectWireframe.jpg)

<!--slide-->

#### Projection onto mobile surface

<iframe src="https://player.vimeo.com/video/268028595" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

<!--slide-->

#### Handling projections overlaps

![](images/semidome_wireframe_no_blending.jpg)

<!--section-->

### Interaction with LivePose

![](images/logos/livepose.png)

<!--slide-->

#### Pose estimation

**Goal**: To estimate the pose of a person in an image by locating special body
points (**keypoints**).

![](images/keypoints_and_people.png)

<!--slide-->

#### Pose estimation in live feeds

<iframe src="images/openpose_walk_0.mp4" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

<!--slide-->

#### Extract additional information

* Coordinates on the floor
* Head tracking
* Simple action detection
* WIP: better action detection for complex actions

<!--slide-->

#### Communication with other tools

* OSC messages
* WebSocket
* WIP: libmapper

<!--section-->

###### Other useful FOSS tools

* [Chataigne](https://benjamin.kuperberg.fr/chataigne/en#home)
* [OSSIA Score](https://ossia.io/)
* [libmapper](https://libmapper.github.io/)
* [webmapper](https://github.com/libmapper/webmapper)
* [OBS](https://obsproject.com/)
* [Godot](https://godotengine.org)
* [Blender](https://www.blender.org/)

<!--section-->

###### Bring your own software!

* OSC
* NDI
* ...

<!--section-->

# Brainstorm

<!--section-->

# Ideas

#### Table 1
* Map your body onto simple objects, from pose detection
* Use proximity of participants to generate effects

#### Table 2
* Mapping the experience from loneliness to the excitement/anxiety of togetherness
* Use 3D scans of spaces at different times
* Use ambient sound

#### Table 3
* Work on clustering and gathering, with different gathering points
* When people get closer, modify the sound
* Display people as shadows/trails

#### Table 4
* Projection onto bodies from pose detection, using abstract video content
* Use pose detection from people outside to generate audio
* Work on proximity/collision detection

<!--section-->

# Getting ideas together

### Interactivity
* Working with pose proximity

### Video content
* Live content

### Sound content
* Sound files

### Physical installation
* Manu & Michal can start with suggestions

<!--section-->
# Technicalities

## Wifi
* ssid: Wifi_CCCB
* username: ISEA2022
* password: iseabcn2022

## Documentation
* https://sat-metalab.gitlab.io/satie
* https://sat-metalab.gitlab.io/splash
* https://sat-metalab.gitlab.io/livepose

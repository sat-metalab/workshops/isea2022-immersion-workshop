ISEA2022 Call for Proposals
===========================

**WORKSHOP AND TUTORIAL**

We invite proposals for half-, full- or two-day workshops and tutorials to take place during two days of ISEA2022. On the one hand, these are intended as opportunities for academics and practitioners to discuss and work on specific topics, while on the other, they are also appropriate for engaging with the general public based on local traditional culture. While workshop organizers are invited to consider the themes of ISEA2022, proposals on all matters relevant to ISEA community and beyond are welcome. Accepted workshops will be announced on the symposium website, and organizers may release their own call for submissions. Workshop organizers may request a presentation slot during the conference to report on the work done in their respective workshops and may submit a one-page abstract of their workshop proposal to be included in the proceedings. There may also be opportunities for exhibiting the outcomes of workshops and tutorials. While all accepted workshops will be provided with a room, specific needs for space and equipment can be further discussed.

Application documents should be delivered to the ISEA2022 Organizer
You should join the official ISEA2022 website and upload all the required documents via online application page.

Required Documents
Application Form

-   Workshop/Tutorial Basic Information
-   Short Biography of the Organizers
-   Abstract
-   Plan of How to Solicit Participation
-   List of Equipment
-   Your preference to possibly exhibit the outcomes of the workshop during the symposium



# To propose workshop and tutorial, submit the proposal of maximum 4 pages on the ISEA2022 template.

\*Should be submitted in PDF format. (Including a URL link of the tutorial website in the PDF file.)
\*All documents should be prepared in English.
\*If you are receiving any support from another institution or under any arrangement with another institution, such as a development plan, and the period of such support or arrangement overlaps with the proposed project, please provide a brief description of the support or arrangement in this form.

For technical questions about Microsoft Word formatting please seek online tutorials. For other questions about your manuscript please contact:  isea2022@uoc.edu



## ISEA2022 Workshop/Tutorial Application Form

-   Workshop/Tutorial Basic Information


| Type                                              | Workshop X  / Tutorial                                             |
| Title of the  Workshop/ Tutorial                  | Creating Spontaneous and Mobile Immersive spaces using FLOSS        |
| Projected Duration                                | Half Day   / Full Day   / Two Days (Workshop Only) X              |
| Name of the Organizers, Presenters & Affiliations | [SAT] Metalab                                                       |
| Rationale / Target audience of the Workshop       | Installation, performance artists, designers, creators              |
| URL of the tutorial website                       | https://sat-metalab.gitlab.io/workshops/isea2022-immersion-workshop |


## Short Biography of the Organizers (within 300 words)

(The text body of short biography should be 12 point. Please fill in maximum 300 words.)

Founded in 2002, Metalab is the research laboratory of the Society for Arts and Technology [SAT]. Metalab's mission is twofold: to stimulate the emergence of innovative immersive experiences and to make their design accessible to artists and creators of immersion through an ecosystem of free software that addresses problems that are not easily solved by existing tools. The software (and recently hardware) developed at the Metalab is often used with other existing technologies to facilitate the mixing of video mapping, 3D audio, telepresence and interaction.


## Abstract

(The text body of Abstract should be 12 point. Please fill in maximum 500 words.)

This workshop is aimed at artists, designers, content creators and other creatives who are interested in creating immersive spaces using low cost and light equipment. We will be using off-the-shelf hardware and open-source software to create an interactive and immersive experience, from scratch, in an impromptu space. While doing so, we will be learning about the tools and the process of getting the basic elements of such undertaking, such as evaluating the available space, setting up projection mapping, calibrating video projectors and sound equipment, setting up cameras for live pose tracking and estimation and employing a software pipeline to glue it all together.

The workshop is based on using exclusively free/libre open source software democratizing what's possible to do with computer vision, computer graphics and spatialized audio technologies that are still marginalized and on the fringes of creative activities.

We will be exploring production pipelines that illustrate the use of software developed by the Metalab in tandem with existing tools that have been seeing wider adoption in recent years. For clarity, we present the proposed toolset in two groups: software developed by the Metalab and other software. 

-   Metalab software:
    -   LivePose - for interpreting live pose estimation using machine learning
    -   Splash - for video mapping
    -   SATIE - for sound rendering and spatialization
-   Other software:
    -   Godot Game Engine - live 3D renderer for interactive content
    -   Chataigne - show control and "glue" for data mapping

The above software can run on low-cost, low-energy hardware such as Raspberry Pi and NVIDIA Jetson single board computers. We believe that putting such hardware and software into creative hands can bring awe an wonder as much, if not more, than the technological bloat that we experience today.


## Plan of How to Solicit Participation

(The text body should be 12 point.)

-   discovery of new tools
-   fostering collaboration
-   FLOSS alternatives
-   free software, low-cost hardware

The call for participation to this workshop will emphasize first and foremost the collaboration, sharing and accessibility aspects from where an art piece can emerge from. Collaboration and sharing aspects are brought firstly by the creation of a single interactive and immersive experience, all participants being encouraged to come with ideas and content and to be open to the suggestions of other participants.

Sharing and accessibility are brought by the use of technologies which are easy to obtain. In addition to cameras, videoprojectors and audio speakers, the workshop makes use of free software as well as low-cost hardware. Some of the software is already well known in the community (Godot, Chataigne), whereas our own software are not as much but they address use cases which are either not possible with other software or involve costly solutions.

The workshop website will have reference to all of the software tools listed, as well as to documentation of the Raspberry Pi and NVIDIA Jetson. It will also link to previous workshops we did around collaborative creation of an immersive experience, forgoing the part about hardware installation.


## Preferred venue

None preferred, we leave it at the discretion of the organizers to find suitable space for the workshop, keeping in mind that its outcome remain in place for the subsequent exhibition, if possible.

## List of Equipment

-   provided by us:
    -   Raspberry Pi
    -   Jetson Xavier NX
    -   audio interface(s)
    -   portable projectors
    -   cameras
    -   cables

-   provided by ISEA
    -   speakers and associated audio cables


## Your preference to possibly exhibit the outcomes of the workshop during the symposium

The workshop will lead to the creation of a uniquely designed interactive audiovisual performance, with the contribution of all of the participants. It could be presented during a short talk, but more importantly it could be made available to the public for the rest of the symposium as an illustration of a collaborative, ephemeral on-site art installation.
